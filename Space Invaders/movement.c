// ADCSWTrigger.c
// Runs on LM4F120/TM4C123
// Provide functions that initialize ADC0 SS3 to be triggered by
// software and trigger a conversion, wait for it to finish,
// and return the result.
// Daniel Valvano
// October 20, 2013

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

#include "tm4c123gh6pm.h"
#include "globals.h"

int M = 0;
int S = 0;
long delta[2] = {2, -2};
long edge[2] = {68, 0};
int dir = 0;

void Move(int index){
	if (Enemy[index].image != SmallExplosion0){
		if(Enemy[index].x==edge[dir]){
			dir^=1;
			EnemyFire=yes;
		}
		Enemy[index].x += delta[dir];
	}
}

void MoveMissile(int index) {
	if(launch==fire&&Player[0].life) {
		Missile[M].x=Player[0].x+8;						//wdith of palyer is 18, 9 is center
		Missile[M].y=Player[0].y-2;
		Missile[M].image= (lev >= 5)? Invis:Missile0;
		Missile[M].life=1;
		Missile[M].p = 0;
		M++;
		M %= 1;
		launch=notfire;
	}
	if(spec_launch==fire&&Player[0].life) {
		Spec_Missile[S].x=Player[0].x+8;						//wdith of palyer is 18, 9 is center
		Spec_Missile[S].y=Player[0].y-2;
		Spec_Missile[S].image= (lev >= 5)? Invis:Missile0;
		Spec_Missile[S].life=1;
		Spec_Missile[S].p = 0;
		S++;
		S %= 1;
		spec_launch=notfire;
	}
	Spec_Missile[index].y-=4;
	if (Spec_Missile[index].y <=0 || Spec_Missile[index].image==SmallExplosion0 ) Spec_Missile[index].life = 0;
	Missile[index].y-=4;
	if (Missile[index].y <=0 || Missile[index].image==SmallExplosion0 ) Missile[index].life = 0;
	
}

void MoveLaser(int index)
{
	int i;
	if(EnemyFire==yes)														//if enemies changed directions, create lasers
	{
		for(i=0;i<4;i++){
			if (Enemy[i].life) {
			Laser[i].x = Enemy[i].x+8;					//enemy ship width =16
			Laser[i].y = Enemy[i].y+4;					//higher the more down enemy ship height =10
			Laser[i].image = (lev >= 6)? Invis:Laser0;
			Laser[i].life = P_HEALTH;
			}
		}
		EnemyFire=no;
	}
	
	Laser[index].y = (Enemy[index].life)? Laser[index].y+4: -5;
	
	if (Laser[index].y >= 55)
		Laser[index].life = 0;
}





//------------Slider------------
// Busy-wait Analog to digital conversion
// Input: none
// Output: 12-bit result of ADC conversion
void Slider(int index){  
	unsigned long result;
  ADC0_PSSI_R = 0x0008;            // 1) initiate SS3
  while((ADC0_RIS_R&0x08)==0){};   // 2) wait for conversion done
  result = ADC0_SSFIFO3_R&0xFFF;   // 3) read result
	#if defined(ANDERS)
	//if(result>=2248 & Player[0].x<64)
	//	result = Player[0].x + 6;
	//else if (result<= 1848 & Player[0].x>2)
	//	result = Player[0].x - 6;
	//else result = Player[0].x;
	result = 66 - result/60.0;
	#elif defined(DAVELLE)
	result /= 63.0;
	#endif
	Player[0].x = result;
  ADC0_ISC_R = 0x0008;             // 4) acknowledge completion
}


void Joystick(int index){  
	unsigned long result;
  ADC0_PSSI_R = 0x0008;            // 1) initiate SS3
  while((ADC0_RIS_R&0x08)==0){};   // 2) wait for conversion done
  result = ADC0_SSFIFO3_R&0xFFF;   // 3) read result
	#if defined(ANDERS)
	if(result>=2248 & Player[0].x<64)
		result = Player[0].x + 6;
	else if (result<= 1848 & Player[0].x>2)
		result = Player[0].x - 6;
	else result = Player[0].x;
	//result = 66 - result/60.0;
	#elif defined(DAVELLE)
	result /= 63.0;
	#endif
	Player[0].x = result;
  ADC0_ISC_R = 0x0008;             // 4) acknowledge completion
}

