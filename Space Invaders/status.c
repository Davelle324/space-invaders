/* STATUS
 * 
 * "check" functions for status
 *
 */
 
#include "tm4c123gh6pm.h"
#include "globals.h"

void CheckLife()				//allows to show animation before dying
{
	for(i=0;i<4;i++)
	{
		if(Enemy[i].image==SmallExplosion0)
			Enemy[i].life /= 2;
		if(Bunker[i].image==SmallExplosion0)
			Bunker[i].life /= 2;
		if(Player[0].image==BigExplosion1)
			Player[0].life /= 2;
		if(Missile[0].image==SmallExplosion0)
			Missile[0].life /= 2;
	}
}

char CheckWin(){
	for (i=0;i<4;i++)
		if(Enemy[i].image != SmallExplosion0)
			return 0;
	return 1;
}

char CheckColl(STyp Sub, STyp Obj, int d) {
	return Obj.x<Sub.x && Sub.x<Obj.x+Obj.w && Sub.y*d>=(Obj.y+Obj.h)*d && Sub.life && Obj.life;
}  


void CheckHit()				//Check to See if Lasers hit or missiles hit
{	
	for(i=0;i<4;i++) {
		if(CheckColl(Laser[i], Player[0], 1))
			KillObject(Player,0);
		for(k=0;k<3;k++)
			if(CheckColl(Laser[i], Bunker[k], 1))	{			//if in proximity of Bunker, change image Laser[i].y>=Bunker.y-3
				Laser[i].image=Laser1;
				Laser[i].life=0;
				ChangeBunker(k);
			}
	}
	
	for(k=0;k<4;k++) {
		for(i=0;i<3;i++) {
			if(CheckColl(Missile[k], Bunker[i], -1))						//destroy missile if hit bunker
				KillObject(Missile, k);
			if(CheckColl(Spec_Missile[k], Bunker[i], -1))						//destroy missile if hit bunker
				KillObject(Spec_Missile, k);
			if(CheckColl(Missile[k], Enemy[i], -1) || CheckColl(Spec_Missile[k], Enemy[i],-1)) {					//if in proximity of enemy make enemies explode
				KillObject(Enemy, i);
				points += Enemy[i].p;
				Enemy[i].p = 0;
				KillObject(Missile,k);
			}
			
			
		}
		if(CheckColl(Missile[k], Enemy[3], -1) || CheckColl(Spec_Missile[k], Enemy[3],-1)){					//if in proximity of enemy make enemies explode
			KillObject(Enemy, 3);
			points += Enemy[3].p;
			Enemy[3].p = 0;
			KillObject(Missile,k);
		}
	}
		
}
