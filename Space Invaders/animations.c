/* ANIMATIONS
 * 
 * Functions for animations
 *
 */
 


#include "globals.h"
#include "Nokia5110.h"
#include "tm4c123gh6pm.h"
void ChangeBunker(int k)
{
	
	 if(Bunker[k].image==Bunker2)
	 {
		KillObject(Bunker, k);
	 }
	 else if(Bunker[k].image==Bunker1)
				Bunker[k].image=Bunker2;
	else if(Bunker[k].image==Bunker0)
		Bunker[k].image=Bunker1;
}


//																//
void KillObject(STyp Object[], int index) {
	Object[index].image = (Object[index].y == 45)?BigExplosion1:SmallExplosion0;
}


// Qouneh's comment: This function renders the image. It is called inside the main loop at 30 Hz. 
void Draw(void){ int i;
  Nokia5110_ClearBuffer();		// clears the Screen array
  for(i=0;i<4;i++){
    if(Enemy[i].life){
     Nokia5110_PrintBMP(Enemy[i].x, Enemy[i].y, Enemy[i].image, 0);	// draw enemy on the Screen array (i.e. buffer)
    }
		if(Player[0].life){
			Nokia5110_PrintBMP(Player[0].x, Player[0].y, Player[0].image, 0);
		}
	
		if(Bunker[i].life){
			Nokia5110_PrintBMP(Bunker[i].x, Bunker[i].y, Bunker[i].image, 0);
		}
		if(Laser[i].life&&Enemy[i].life){														//if enemy and corresponind laser are alive, display
     Nokia5110_PrintBMP(Laser[i].x, Laser[i].y, Laser[i].image, 0);	// draw enemy on the Screen array (i.e. buffer)
    }
		if(Missile[i].life && Player[0].life){
			Nokia5110_PrintBMP(Missile[i].x, Missile[i].y, Missile[i].image, 0);
		}
		if(Spec_Missile[i].life && Player[0].life){
			Nokia5110_PrintBMP(Spec_Missile[i].x, Spec_Missile[i].y, Spec_Missile[i].image, 0);
		}
		
  }
  Nokia5110_DisplayBuffer();      // draw buffer Screen on the LCD
	if(!Player[0].life)
		{Nokia5110_SetCursor(1, 2);
			Nokia5110_OutString("Nice Try !");
			Nokia5110_SetCursor(1, 3);
			Nokia5110_OutString("Score:");
			//Nokia5110_SetCursor(1, 4);
			//Nokia5110_OutUDec(lev);
			Nokia5110_SetCursor(2, 4);
			Nokia5110_OutUDec(points);
		}
}
