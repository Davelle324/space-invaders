
//PORBLEMS??
// add scores
// 
// http://www.spaceinvaders.de/
// sounds at http://www.classicgaming.cc/classics/spaceinvaders/sounds.php
// http://www.classicgaming.cc/classics/spaceinvaders/playguide.php
/* This example accompanies the books
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013

   "Embedded Systems: Introduction to Arm Cortex M Microcontrollers",
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2013

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
 
 // ******* Required Hardware I/O connections*******************
// Slide pot pin 1 connected to ground
// Slide pot pin 2 connected to PE2/AIN1
// Slide pot pin 3 connected to pne side of the 1k resistor
// other side of the 1k resistor is connected to +3.3V 
// fire button connected to PE0
// special weapon fire button connected to PE1
// 8*R resistor DAC bit 0 on PB0 (least significant bit)
// 4*R resistor DAC bit 1 on PB1
// 2*R resistor DAC bit 2 on PB2
// 1*R resistor DAC bit 3 on PB3 (most significant bit)
// LED on PB4
// LED on PB5

// Blue Nokia 5110
// ---------------
// Signal        (Nokia 5110) LaunchPad pin
// Reset         (RST, pin 1) connected to PA7
// SSI0Fss       (CE,  pin 2) connected to PA3
// Data/Command  (DC,  pin 3) connected to PA6
// SSI0Tx        (Din, pin 4) connected to PA5
// SSI0Clk       (Clk, pin 5) connected to PA2
// 3.3V          (Vcc, pin 6) power
// back light    (BL,  pin 7) not connected, consists of 4 white LEDs which draw ~80mA total
// Ground        (Gnd, pin 8) ground

// Red SparkFun Nokia 5110 (LCD-10168)
// -----------------------------------
// Signal        (Nokia 5110) LaunchPad pin
// 3.3V          (VCC, pin 1) power
// Ground        (GND, pin 2) ground
// SSI0Fss       (SCE, pin 3) connected to PA3
// Reset         (RST, pin 4) connected to PA7
// Data/Command  (D/C, pin 5) connected to PA6
// SSI0Tx        (DN,  pin 6) connected to PA5
// SSI0Clk       (SCLK, pin 7) connected to PA2
// back light    (LED, pin 8) not connected, consists of 4 white LEDs which draw ~80mA total

#include "Nokia5110.h"
#include "PLL.h"
#include "tm4c123gh6pm.h"
#include "globals.h"

//initialize interrupt for switch//
//void Int_Init(void){                          
//	SYSCTL_RCGCGPIO_R |= 0x00000010;	// (a) activate clock for Port A, bit 0
//	while((SYSCTL_PRGPIO_R&0x00000010) == 0){}; // allow time for clock to start. Wait for status bit to be true
//  GPIO_PORTE_DIR_R &= ~0x01;    // (c) make PA7 in 
//  GPIO_PORTE_AFSEL_R &= ~0x01;  //     disable alt funct on PA7
//  GPIO_PORTE_DEN_R |= 0x01;     //     enable digital I/O on PA7
//  GPIO_PORTE_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
//  GPIO_PORTE_AMSEL_R = 0;       //     disable analog functionality on PF
//  //GPIO_PORTB_PUR_R |= 0x10;     //     have own pull up resistor
//		
//	// Initialization of interrupt registers
//  GPIO_PORTE_IS_R &= ~0x01;     // (d) PA7 is edge-sensitive
//  GPIO_PORTE_IBE_R &= ~0x01;    //     PA7 is not both edges
//  GPIO_PORTE_IEV_R &= ~0x01;    //     PA7 falling edge event
//  GPIO_PORTE_ICR_R = 0x01;      // (e) clear flag7
//  GPIO_PORTE_IM_R |= 0x01;      // (f) arm interrupt on PA7
//  NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|0x00A00000; // (g) assign this interrupt a priority 5
//  NVIC_EN0_R = 0x00000010;      // (h) enable interrupt bit 0 in NVIC EN0 register
//	__enable_irq();								// (i) Clears the I bit
//}
//	int x=0;
//void GPIOE_Handler(void){
//  GPIO_PORTE_ICR_R = 0x01;      // acknowledge flag7
//	x++;										//set global variable =1
//}

void SysTick_Handler(void){						//launches missiles
	//32 Hz section
	if((GPIO_PORTE_DATA_R & FIRE_PIN))
	{
		launch=fire;
	}
	if((GPIO_PORTE_DATA_R & SPEC_PIN))
		spec_launch=fire;
	
	if (Player[0].image != BigExplosion1)
	{
		Player[0].movement(0);
		GPIO_PORTE_DATA_R&=~0x10;	
	}
		//GPIO_PORTE_DATA_R|=0x10;				//if player dies, display red light..doesnt work rn
	Draw();
	
	if (CheckWin()) {
		EndGame();
	}
	
	//10Hz section
	if (Fqz++ >= 2) {
		for(i=0;i<4;i++) {
			Enemy[i].movement(i);										//move all animations
		  Laser[i].movement(i);
		  MoveMissile(i);
	  }
		CheckHit();										//Check if lasers/missiles hit anything
    //Draw();												//displays stuff
		CheckLife();									//set lives = 0 here, so animation can play out first
		Fqz = 0;
	}
}

int main(void){
	int i;
  PLL_Init();                   // set system clock to 80 MHz
  Random_Init(1);
  Nokia5110_Init();
  Nokia5110_ClearBuffer();
	Nokia5110_DisplayBuffer();      // draw buffer
	PortE_Init();										//initialize button
  ADC0_InitSWTriggerSeq3_Ch1();         // ADC initialization PE2/AIN1
	SysTick_Init(5000000);						//initialize sysytick interrupt
	
		__disable_irq();
	
	Nokia5110_SetCursor(2, 2); 
	Nokia5110_OutString("Invaders!!");
	Nokia5110_SetCursor(3, 1); 
	Nokia5110_OutString("Space");
  Nokia5110_SetCursor(0, 0); 
	Nokia5110_OutString("Welcome to");
	for (i=0;i<40000000;i++);
	Nokia5110_Clear();
	Nokia5110_SetCursor(1, 5); 
	Nokia5110_OutString("B: Joystick");
	Nokia5110_SetCursor(1, 4); 
	Nokia5110_OutString("A: Slider");
	Nokia5110_SetCursor(0, 3); 
	Nokia5110_OutString("Choose Mode:");
	
	while(!(GPIO_PORTE_DATA_R & SPEC_PIN) && !(GPIO_PORTE_DATA_R & FIRE_PIN));
	if(GPIO_PORTE_DATA_R & SPEC_PIN)
		Player[0].movement = Slider;
	else
		Player[0].movement = Joystick;
	
		__enable_irq();
	
  Init(lev);
  Draw();
  while(1){
	}
}

void SystemInit(void)
{
		// Disable interrupts
		__disable_irq();
	
		/* Grant coprocessor access */ 
    /* This is required since TM4C123G has a floating point coprocessor. */ 
    NVIC_CPAC_R |= 0x00F00000;
} 


