
/* INITS
 * 
 * all initialization functions
 *
 */
 

#include "globals.h"
#include "Nokia5110.h"
#include "tm4c123gh6pm.h"

void EndGame(){
	Nokia5110_SetCursor(2, 1); 
		Nokia5110_OutString("You win!!");
	
		Nokia5110_SetCursor(3, 2);
		Nokia5110_OutString("Level: ");
		Nokia5110_SetCursor(2, 3);
		Nokia5110_OutUDec(lev);
		Nokia5110_SetCursor(3, 4);
		Nokia5110_OutString("in: 3");
		if (temp++ >= 16 && temp < 32) {
		Nokia5110_SetCursor(3, 4);
		Nokia5110_OutString("in: 2");
		}
		else if (temp >= 32 && temp <48) {
		Nokia5110_SetCursor(3, 4);
		Nokia5110_OutString("in: 1");
		}
		if (temp >= 48) {
			Init(++lev);
			temp = 0;
		}
}
// Initialization of Ships/Bunkers//
void Init(char level){ 
	int i;
	//Enemies//
			for(i=0;i<4;i++){
				Enemy[i].x = 16*i;
				Enemy[i].y = 10;					//higher the more down 
				Enemy[i].image = SmallExplosion0;
				Enemy[i].life = 0;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Enemy[i].p = 10;
				Laser[i].life = 0;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
			
			//Bunkers//
			for(i=0;i<3;i+=2) {
				Bunker[i].x = 32*i;
				Bunker[i].y = 35;					//higher the more down 
				Bunker[i].image = Bunker2;
				Bunker[i].life = 0;
				Bunker[i].h = -3;
				Bunker[i].w = 18;
				Bunker[i].p = 0;
			}
	switch (level) {
		case 2:
			//Enemies//
			for(i=0;i<4;i+=2){
				Enemy[i].x = 16*i;
				Enemy[i].y = 10;					//higher the more down 
			
				Enemy[i].life = E_HEALTH;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Enemy[i].p = 10;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
				Enemy[0].image = SmallEnemy10PointA;
				Enemy[2].image = SmallEnemy10PointA;
			//Bunkers//
			for(i=0;i<3;i++) {
				Bunker[i].x = 32*i;
				Bunker[i].y = 35;					//higher the more down 
				Bunker[i].image = Bunker0;
				Bunker[i].life = B_HEALTH;
				Bunker[i].h = -3;
				Bunker[i].w = 18;
			}
		break;
		case 3:
			//Enemies//
			for(i=0;i<3;i++){
				Enemy[i].x = 16*i;
			
				Enemy[i].life = E_HEALTH;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
				Enemy[0].y = 10;					//higher the more down 
				Enemy[1].y = 16;
				Enemy[2].y = 10;
				Enemy[0].image = SmallEnemy10PointB;
				Enemy[1].image = SmallEnemy20PointA;
				Enemy[2].image = SmallEnemy10PointB;
				Enemy[0].p = 10;
				Enemy[1].p = 20;
				Enemy[2].p = 10;
			//Bunkers//
			for(i=0;i<3;i+=2) {
				Bunker[i].x = 32*i;
				Bunker[i].y = 35;					//higher the more down 
				Bunker[i].image = Bunker0;
				Bunker[i].life = B_HEALTH;
				Bunker[i].h = -3;
				Bunker[i].w = 18;
			}
			Bunker[1].life = 0;
			KillObject(Bunker, 1);
		break;
		case 4:
			//Enemies//
			for(i=0;i<4;i++){
				Enemy[i].x = 16*i;
				
				
				
				Enemy[i].life = E_HEALTH;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
				Enemy[0].image = SmallEnemy20PointB;
				Enemy[1].image = SmallEnemy30PointB;
				Enemy[2].image = SmallEnemy30PointB;
				Enemy[3].image = SmallEnemy20PointB;
				Enemy[0].y = 10;					//higher the more down 
				Enemy[1].y = 16;
				Enemy[2].y = 10;
				Enemy[3].y = 16;
				Enemy[0].p = 20;
				Enemy[1].p = 30;
				Enemy[2].p = 30;
				Enemy[3].p = 20;
			for(i=0;i<3;i++) {
				Bunker[i].x = 32*i;
				Bunker[i].y = 35;					//higher the more down 
				Bunker[i].image = Bunker0;
				Bunker[i].life = B_HEALTH;
				Bunker[i].h = -3;
				Bunker[i].w = 18;
			}
			Bunker[2].life = 0;
			KillObject(Bunker, 2);
			Bunker[0].life = 0;
			KillObject(Bunker, 0);
		break;
		case 5:
			for(i=0;i<4;i++){
				Enemy[i].x = 16*i;
				Enemy[i].y = 10;					//higher the more down 
				Enemy[0].image = SmallEnemy30PointB;
				Enemy[1].image = SmallEnemy30PointA;
				Enemy[2].image = SmallEnemy30PointA;
				Enemy[3].image = SmallEnemy30PointB;
				Enemy[0].p = 30;
				Enemy[1].p = 30;
				Enemy[2].p = 30;
				Enemy[3].p = 30;
				Enemy[i].life = E_HEALTH;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
			Bunker[1].life = 0;
			KillObject(Bunker, 1);
			break;
		default:
			//Enemies//
			for(i=0;i<4;i++){
				Enemy[i].x = 16*i;
				Enemy[i].y = 10;					//higher the more down 
				Enemy[i].image = Invis;
				Enemy[i].life = E_HEALTH;
				Enemy[i].h = 3;
				Enemy[i].w = 16;
				Enemy[i].p=30;
				Laser[i].movement = MoveLaser;
				Enemy[i].movement = Move;
			}
		}
		
		
	//Player Ship//
	Player[0].y=45;							//lowest is 45				//30-40
	Player[0].image = PlayerShip0;
	Player[0].life= P_HEALTH;
	Player[0].h = -3;
	Player[0].w = 18;
	Player[0].p = 0;
	
	
}

void SysTick_Init(unsigned long period){											//REMEMBER TO CALL THIS FUNCTION IN MAIN WITH CLOCK SPEED INSIDE
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;// reload value
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x00FFFFFF)|0x40000000; // priority 2		|'d with 0x40000000 to set priority to 2, sets bit 2 high in bits 31-29
                              // enable SysTick with core clock and interrupts
  NVIC_ST_CTRL_R = 0x07;			//set to 7 instead of 5 because enabling the interrupt

	__enable_irq();								// (i) Clears the I bit
}

void PortE_Init(void){ 
	volatile unsigned long delay;
   SYSCTL_RCGCGPIO_R |= 0x10   ;     // 1) activate clock for Port B, write a 1 in R1 check data sheet
	while((SYSCTL_PRGPIO_R&0x00000010) == 0){}; // allow time for clock to start. Wait for status bit to be true
  GPIO_PORTE_AMSEL_R = 0x00;        // 3) disable analog on PB
  GPIO_PORTE_PCTL_R = 0x00000000;   // 4) PCTL GPIO on PB4-0
  GPIO_PORTE_DIR_R &= ~0x03;          // 5) Set all Port B outputs high
  GPIO_PORTE_AFSEL_R = 0x00;        // 6) disable alt funct on PB7-0
  //GPIO_PORTE_PUR_R |= 0x10;          
  GPIO_PORTE_DEN_R |= 0x13;          // 7) enable PB7-PB0 Outputs
	GPIO_PORTE_DATA_R&=~0x10;	
}

// This initialization function sets up the ADC according to the
// following parameters.  Any parameters not explicitly listed
// below are not modified:
// Max sample rate: <=125,000 samples/second
// Sequencer 0 priority: 1st (highest)
// Sequencer 1 priority: 2nd
// Sequencer 2 priority: 3rd
// Sequencer 3 priority: 4th (lowest)
// SS3 triggering event: software trigger
// SS3 1st sample source: Ain1 (PE2)
// SS3 interrupts: flag set on completion but no interrupt requested
void ADC0_InitSWTriggerSeq3_Ch1(void){ 
	volatile unsigned long delay;  
	SYSCTL_RCGCGPIO_R |= 0x10;     // 1) activate clock for Port E
	while((SYSCTL_PRGPIO_R&0x10) == 0){}; // allow time for clock to start. Wait for status bit to be true
	GPIO_PORTE_DIR_R &= ~0x04;      // 2) make PE2 input
  GPIO_PORTE_AFSEL_R |= 0x04;     // 3) enable alternate function on PE2
  GPIO_PORTE_DEN_R &= ~0x04;      // 4) disable digital I/O on PE2
  GPIO_PORTE_AMSEL_R |= 0x04;     // 5) enable analog function on PE2
	// Changed to new register name SYSCTL_RCGCADC_R. 
  //SYSCTL_RCGC0_R |= 0x00010000;   // 6) activate ADC0 
  //delay = SYSCTL_RCGC2_R;
	SYSCTL_RCGCADC_R |= 0x01;   // 6) activate ADC0
	// These 4 delay statements allow the clock time to stabilize. Simulation did not work without them.
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;				
			
	// Changed to new register name ADC0_PC_R.
	//  SYSCTL_RCGC0_R &= ~0x00000300;  // 7) configure for 125K 		
	ADC0_PC_R = 0x01;               // 7) configure for 125K 
  ADC0_SSPRI_R = 0x0123;          // 8) Sequencer 3 is highest priority
  ADC0_ACTSS_R &= ~0x0008;        // 9) disable sample sequencer 3
  ADC0_EMUX_R &= ~0xF000;         // 10) seq3 is software trigger
  ADC0_SSMUX3_R = (ADC0_SSMUX3_R&0xFFFFFFF0)+1; // 11) channel Ain1 (PE2)
  ADC0_SSCTL3_R = 0x0006;         // 12) no TS0 D0, yes IE0 END0
  ADC0_ACTSS_R |= 0x0008;         // 13) enable sample sequencer 3
}


void ADC0_InitSWTriggerSeq3_Ch9(void){ 
	volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000010;   // 1) activate clock for Port E
  delay = SYSCTL_RCGC2_R;         //    allow time for clock to stabilize
  GPIO_PORTE_DIR_R &= ~0x08;      // 2) make PE4 input
  GPIO_PORTE_AFSEL_R |= 0x08;     // 3) enable alternate function on PE4
  GPIO_PORTE_DEN_R &= ~0x08;      // 4) disable digital I/O on PE4
  GPIO_PORTE_AMSEL_R |= 0x08;     // 5) enable analog function on PE4
  SYSCTL_RCGC0_R |= 0x0002;   // 6) activate ADC1
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;       // extra time to stabilize
	delay = SYSCTL_RCGCADC_R;						
	ADC1_PC_R = 0x01;               // 7) configure for 125K 
  ADC1_SSPRI_R = 0x0123;          // 8) Sequencer 3 is highest priority
  ADC1_ACTSS_R &= ~0x0008;        // 9) disable sample sequencer 3
  ADC1_EMUX_R &= ~0xF000;         // 10) seq3 is software trigger
  ADC1_SSMUX3_R &= ~0x000F;       // 11) clear SS3 field
  ADC1_SSMUX3_R += 9;             //    set channel Ain9 (PE4)
  ADC1_SSCTL3_R = 0x0006;         // 12) no TS0 D0, yes IE0 END0
  ADC1_ACTSS_R |= 0x0008;         // 13) enable sample sequencer 3

}

