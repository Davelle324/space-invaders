 

/* GLOBALS
 * 
 * Function prototypes, compiler directives, and global variables
 * 
 * IMPORTANT
 * global variables should be here as extern <type> <name>;
 * ALL initializations should happen in globals.c or images.c
 *
 */
 
#define P_HEALTH 1048576
#define B_HEALTH 256
#define E_HEALTH 256

#if defined(ANDERS)
#define FIRE_PIN 0x02
#define SPEC_PIN 0x01

#elif defined(DAVELLE) 
#define FIRE_PIN 0x01
#define SPEC_PIN 0x02
#endif

typedef struct State STyp;

void EndGame(void);
void Init(char);
void ADC0_InitSWTriggerSeq3_Ch1(void);
void ADC0_InitSWTriggerSeq3_Ch9(void);
void PortE_Init(void);
void SysTick_Init(unsigned long);
void Random_Init(int);


void Move(int index);
void Slider(int index);
void Joystick(int index);
void MoveLaser(int index);
void MoveMissile(int index);

void CheckHit(void);
void CheckLife(void);
char CheckWin(void);
char CheckColl(STyp, STyp, int);

void ChangeBunker(int);
void Draw(void);
void KillObject(STyp *, int index);


void Delay100ms(unsigned long);


struct State {
  unsigned long x;      // x coordinate
  long y;      // y coordinate
  const unsigned char *image; // ptr->image
  long life;            // 0=dead, 1=alive
	long w;
	long h;
	short p;
	void (*movement)(int);
};          

extern STyp Enemy[4],Player[1],Bunker[3],Laser[4],Missile[4], Spec_Missile[4];;

extern char Fqz;

extern int temp;

extern char i, k;

extern unsigned short points;
extern char lev;

enum switched{yes,no};									//shoot lasers
extern enum switched EnemyFire;
enum Missile{fire,notfire};
extern enum Missile launch, spec_launch;
enum kill{kill,notkill};
extern enum kill hit;


// *************************** Images ***************************
// enemy ship that starts at the top of the screen (arms/mouth closed)
// width=16 x height=10
extern const unsigned char SmallEnemy30PointA[];

extern const unsigned char Test[];

extern const unsigned char Invis[];

// enemy ship that starts at the top of the screen (arms/mouth open)
// width=16 x height=10
extern const unsigned char SmallEnemy30PointB[];
// enemy ship that starts in the middle of the screen (arms together)
// width=16 x height=10
extern const unsigned char SmallEnemy20PointA[];
// enemy ship that starts in the middle of the screen (arms apart)
// width=16 x height=10
extern const unsigned char SmallEnemy20PointB[];
// enemy ship that starts at the bottom of the screen (arms down)
// width=16 x height=10
extern const unsigned char SmallEnemy10PointA[];
// enemy ship that starts at the bottom of the screen (arms up)
// width=16 x height=10
extern const unsigned char SmallEnemy10PointB[];
// image of the player's ship
// includes two blacked out columns on the left and right sides of the image to prevent smearing when moved 2 pixels to the left or right
// width=18 x height=8
extern const unsigned char PlayerShip0[];
// small, fast bonus enemy that occasionally speeds across the top of the screen after enough enemies have been killed to make room for it
// includes two blacked out columns on the left and right sides of the image to prevent smearing when moved 2 pixels to the left or right
// width=20 x height=7
extern const unsigned char SmallEnemyBonus0[];
// small shield floating in space to cover the player's ship from enemy fire (undamaged)
// width=18 x height=5
extern const unsigned char Bunker0[];
// small shield floating in space to cover the player's ship from enemy fire (moderate generic damage)
// width=18 x height=5
extern const unsigned char Bunker1[];
// small shield floating in space to cover the player's ship from enemy fire (heavy generic damage)
// width=18 x height=5
extern const unsigned char Bunker2[];
// blank space used to cover a bunker that is destroyed
// width=18 x height=5
extern const unsigned char Bunker3[];
// large explosion that can be used upon the demise of the player's ship (first frame)
// width=18 x height=8
extern const unsigned char BigExplosion0[];
// large explosion that can be used upon the demise of the player's ship (second frame)
// width=18 x height=8
extern const unsigned char BigExplosion1[];
// small explosion best used for the demise of an enemy
// width=16 x height=10
extern const unsigned char SmallExplosion0[];
// blank space following the small explosion for the demise of an enemy
// width=16 x height=10
extern const unsigned char SmallExplosion1[];
// a missile in flight
// includes one blacked out row on the top, bottom, and right of the image to prevent smearing when moved 1 pixel down, up, or left
// width=4 x height=9
extern const unsigned char Missile0[];
// a missile in flight
// includes one blacked out row on the top, bottom, and left of the image to prevent smearing when moved 1 pixel down, up, or right
// width=4 x height=9
extern const unsigned char Missile1[];
// blank space to cover a missile after it hits something
// width=4 x height=9
extern const unsigned char Missile2[];
// a laser burst in flight
// includes one blacked out row on the top and bottom of the image to prevent smearing when moved 1 pixel up or down
// width=2 x height=9
extern const unsigned char Laser0[];
// blank space to cover a laser after it hits something
// width=2 x height=9
extern const unsigned char Laser1[];
